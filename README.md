# Aplicação Overview
Aplicação simples de CRUD realizando todos procedimentos diretamente no Mongo Atlas

para iniciar o projeto basta baixar a imagem que está disponibilizada no meu DockerHub docker pull caio10/deploy-kubernetes-crud:latest, fiz esse passo a mais para facilitar na etapa de deploy.

# Conexão com Mongo Atlas

para a conexão eu utilizei o https://cloud.mongodb.com/ criei um novo cluster não versão free e o arquivo de conexão se encontra em /models/db.js


Porem deixei uma segunda alternativa que seria se conectando ao banco atraves do proprio container subindo tanto o mongo quanto Redis atraves do docker-compose

# Gerar token DigitalOcean
Apos logar vamos no menu esquerdo em API e gerar o token para conseguirmos criar recursos na Cloud.

# Terraform
na pasta do terraform tenho dois arquivos o main.tf contendo as informações necessarias configuração do Cluster Kubernetes inclusive o token que permite criar e gerenciar recursos na DigitalOcean via IaC e o segundo arquivo é um arquvo de varaiveis para as informações(informações sensiveis tambem como por exemplo o token da DigitalOcean) não ficar exposta. No proprio Terraform já foi provisionado o Cluster em autoscaling parar cria o cluster bastar rodar os seguintes comandos Terraform init e Terraform apply. Só não deixei esse passo na pipeline por conta que desabilitei meu token

# k8s
Na pasta K8s temos o nosso arquivo de deployment que é responsavel por realizar o deploy dessa aplicação que buildei com o Docker e utilizei um service do tipo LoadBalancer para que consiga acessar via endereço url no caso vai ser um endreço ip que a aplicação ficara disponivel, porem esse endereço vai ser via IP, caso fosse utilizar uma URL por exemplo: https://app.crud.job, teria que utilizar um gerenciador de DNS para disponibilizar esse endereço e o proprio serviço da Cloud o LoadBalancer.

# Build
build - realiza todo o build da aplicação e envia para o meu DockerHub, Poderia optar para enviar para o Registry da DigitalOcean mas mandei para o HUB mesmo.

# Deployment DigitalOcean
Deploy - apos o Cluster criado vamos baixar o Kube.config importar no nosso ambiente atual na nossa maquina por exemplo e executar um kubectl apply -f deployment.yml, esse deploy vai ser aplicado dirtetamente no nosso cluster da DigitalOcean talvez precise realizar uma autenticação via Doctl ou informar o token da DigitalOcean para conseguir realizar o deploy nesse cluster.







