variable "digital_ocean_token" {
  default = "dop_v1_0478d3e87e63369e27aa6661ab1be7e9b72f4dd5751617affed1f4bebad1a90c"
}

variable "digital_ocean_cluster_name" {
  default = "cluster"
}

variable "digital_ocean_cluster_region" {
  default = "nyc3"
}

variable "digital_ocean_cluster_version" {
  default = "1.27.2-do.0"
}

variable "digital_ocean_cluster_pool_name" {
  default = "autoscale-worker-pool"
}

variable "digital_ocean_cluster_pool_size" {
  default = "s-1vcpu-2gb"
}