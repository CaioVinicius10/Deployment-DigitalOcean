terraform {
  required_version = "1.2.2"
}

terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

provider "digitalocean" {
  token = var.digital_ocean_token
}


#cluster kubernetes
resource "digitalocean_kubernetes_cluster" "cluster" {
  name    = var.digital_ocean_cluster_name
  region  = var.digital_ocean_cluster_region
  version = var.digital_ocean_cluster_version

  node_pool {
    name       = var.digital_ocean_cluster_pool_name
    size       = var.digital_ocean_cluster_pool_size
    auto_scale = true
    min_nodes  = 1
    max_nodes  = 2
  }
}

